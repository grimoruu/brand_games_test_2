from typing import Sequence

from sqlalchemy import Row, delete, exists, insert, select, update

from core.services.db_service import BaseDAO
from db.models import Task


class TaskServiceDAO(BaseDAO):

    def __init__(self) -> None:
        self._db = self.get_session()

    async def check_user(self, user_id: int) -> bool:
        query = select(exists().where(Task.user_id == user_id))
        result = await self._db.execute(query)
        return result.scalar_one()

    async def create_task(self, user_id: int, title: str, description: str | None) -> None:
        query = insert(Task).values(user_id=user_id, title=title, description=description)
        await self._db.execute(query)

    async def update_task(self, task_id: int, user_id: int, title: str, description: str | None) -> None:
        query = (
            update(Task).where(Task.user_id == user_id, Task.id == task_id).values(title=title, description=description)
        )
        await self._db.execute(query)

    async def delete_task(self, task_id: int) -> None:
        query = delete(Task).where(Task.id == task_id)
        await self._db.execute(query)

    async def select_tasks(self, user_id: int) -> Sequence[Row]:
        query = select(Task.id, Task.title, Task.description).select_from(Task).where(Task.user_id == user_id)
        result = await self._db.execute(query)
        return result.fetchall()
