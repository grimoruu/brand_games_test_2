from pydantic import BaseModel


class TasksSchemaResponse(BaseModel):
    task_id: int
    title: str
    description: str
