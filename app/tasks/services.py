from app.tasks.dao import TaskServiceDAO
from app.tasks.schemas import TasksSchemaResponse


class TaskService:
    def __init__(self) -> None:
        self._user_dao = TaskServiceDAO()

    async def get_tasks_service(self, user_id: int) -> list[TasksSchemaResponse]:
        tasks = await self._user_dao.select_tasks(user_id)
        result = [TasksSchemaResponse(task_id=row.id, title=row.title, description=row.description) for row in tasks]
        return result

    async def create_task_service(self, user_id: int, title: str, description: str | None) -> None:
        await self._user_dao.create_task(user_id, title, description)

    async def update_task_service(self, task_id: int, user_id: int, title: str, description: str | None) -> None:
        await self._user_dao.update_task(task_id, user_id, title, description)

    async def delete_task_service(self, task_id: int) -> None:
        await self._user_dao.delete_task(task_id)

    async def check_user_service(self, user_id: int) -> bool:
        return await self._user_dao.check_user(user_id)
