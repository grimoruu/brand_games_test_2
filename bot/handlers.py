from aiogram import Bot, Dispatcher, types
from aiogram.filters import Command
from aiogram.fsm.context import FSMContext
from aiogram.types import InlineKeyboardButton, InlineKeyboardMarkup

from app.tasks.services import TaskService
from bot.kb import Kb, KbCls
from bot.states import MenuState
from bot.text import TextMessages
from core.config.settings import settings

bot = Bot(token=settings.tg_token.token)
dp = Dispatcher()


@dp.message(Command("start"))
async def cmd_start(message: types.Message) -> None:
    user_id = message.from_user.id
    service = TaskService()
    user = await service.check_user_service(user_id)
    if user:
        await message.answer(TextMessages.WELCOME_BACK_MESSAGE, reply_markup=Kb.menu_kb)
    else:
        await message.answer(TextMessages.START_MESSAGE, reply_markup=Kb.menu_kb)


@dp.message(Command("menu"))
async def cmd_menu(message: types.Message) -> None:
    await message.answer(TextMessages.SIMPLE_QUESTION, reply_markup=Kb.menu_kb)


@dp.callback_query(lambda callback_query: callback_query.data == "add_task")
async def add_task(callback_query: types.CallbackQuery, state: FSMContext) -> None:
    await state.set_state(MenuState.ADDING_TITLE)
    await bot.send_message(callback_query.from_user.id, TextMessages.ADD_TASK_TITLE)


@dp.message(MenuState.ADDING_TITLE)
async def process_add_task_title(message: types.Message, state: FSMContext) -> None:
    await state.update_data(title=message.text)
    await state.set_state(MenuState.ADDING_DESCRIPTION)
    await bot.send_message(message.from_user.id, TextMessages.ADD_TASK_DESCRIPTION)


@dp.message(MenuState.ADDING_DESCRIPTION)
async def process_add_task_description(message: types.Message, state: FSMContext) -> None:
    await state.update_data(description=message.text)
    await state.set_state(MenuState.CONFIRM_TASK_ADDITION)
    await bot.send_message(
        message.from_user.id, TextMessages.CONFIRM_TASK_ADDITION, reply_markup=Kb.confirm_task_addition_kb
    )


@dp.callback_query(
    lambda callback_query: callback_query.data in ["save_task", "cancel_task"], MenuState.CONFIRM_TASK_ADDITION
)
async def handle_confirmation_buttons(callback_query: types.CallbackQuery, state: FSMContext) -> None:
    if callback_query.data == "save_task":
        data = await state.get_data()
        user_id = callback_query.from_user.id
        title = data.get("title")
        description = data.get("description")
        await bot.send_message(callback_query.from_user.id, TextMessages.TASK_ADDED_SUCCESSFULLY)
        service = TaskService()
        await service.create_task_service(user_id, title, description)
        await cmd_menu(callback_query.message)
    elif callback_query.data == "cancel_task":
        await bot.send_message(callback_query.from_user.id, TextMessages.TASK_ADDITION_CANCELLED)
        await cmd_menu(callback_query.message)


@dp.callback_query(lambda callback_query: callback_query.data == "tasks_list")
async def show_tasks_list(callback_query: types.CallbackQuery) -> None:
    user_id = callback_query.from_user.id
    service = TaskService()
    tasks = await service.get_tasks_service(user_id)
    if tasks:
        buttons = []
        for task in tasks:
            button_text = f"{task.title}"
            button_data = f"task_{task.task_id}_{task.title}_{task.description}"
            buttons.append([InlineKeyboardButton(text=button_text, callback_data=button_data)])
        add_btn = Kb.return_to_menu_kb
        buttons.append(add_btn.inline_keyboard[0])
        keyboard = InlineKeyboardMarkup(inline_keyboard=buttons)
        await bot.send_message(callback_query.from_user.id, TextMessages.CHOOSE_TASK_TO_VIEW, reply_markup=keyboard)
    else:
        await bot.send_message(callback_query.from_user.id, TextMessages.TASK_LIST_EMPTY)
        await cmd_menu(callback_query.message)


@dp.callback_query(lambda callback_query: callback_query.data.startswith("task_"))
async def show_task_info(callback_query: types.CallbackQuery) -> None:
    task_data = callback_query.data.split("_")
    task_id = task_data[1]
    task_title = task_data[2]
    task_description = task_data[3]
    message_text = f"Название задачи: {task_title}\n" f"Описание задачи: {task_description}"
    await bot.send_message(callback_query.from_user.id, message_text)
    keyboard = KbCls.tasks_list_kb(int(task_id))
    await bot.send_message(callback_query.from_user.id, TextMessages.WHAT_TO_DO_WITH_TASK, reply_markup=keyboard)


@dp.callback_query(lambda callback_query: callback_query.data.startswith("delete_task"))
async def delete_task(callback_query: types.CallbackQuery) -> None:
    task_id = int(callback_query.data.split("_")[2])
    service = TaskService()
    await service.delete_task_service(task_id)
    await bot.send_message(callback_query.from_user.id, TextMessages.TASK_DELETED_SUCCESSFULLY)
    await show_tasks_list(callback_query)


@dp.callback_query(lambda callback_query: callback_query.data.startswith("edit_task"))
async def edit_task(callback_query: types.CallbackQuery, state: FSMContext) -> None:
    task_id = int(callback_query.data.split("_")[2])
    await state.update_data(task_id=task_id)
    await state.set_state(MenuState.EDITING_TASK_TITLE)
    await bot.send_message(callback_query.from_user.id, TextMessages.ENTER_NEW_TASK_TITLE)


@dp.message(MenuState.EDITING_TASK_TITLE)
async def process_edit_task_title(message: types.Message, state: FSMContext) -> None:
    task_id = (await state.get_data()).get("task_id")
    await state.update_data(title=message.text)
    await state.set_state(MenuState.EDITING_TASK_DESCRIPTION)
    await state.update_data(task_id=task_id)
    await bot.send_message(message.from_user.id, TextMessages.ENTER_NEW_TASK_DESC)


@dp.message(MenuState.EDITING_TASK_DESCRIPTION)
async def process_edit_task_description(message: types.Message, state: FSMContext) -> None:
    task_id = (await state.get_data()).get("task_id")
    await state.update_data(description=message.text)
    await state.set_state(MenuState.CONFIRM_TASK_UPDATE)
    await state.update_data(task_id=task_id)
    await bot.send_message(
        message.from_user.id, TextMessages.CONFIRM_TASK_UPDATE, reply_markup=Kb.confirm_task_update_kb
    )


@dp.callback_query(
    lambda callback_query: callback_query.data in ["save_updated_task", "cancel_updated_task"],
    MenuState.CONFIRM_TASK_UPDATE,
)
async def handle_update_confirmation_buttons(callback_query: types.CallbackQuery, state: FSMContext) -> None:
    if callback_query.data == "save_updated_task":
        data = await state.get_data()
        user_id = callback_query.from_user.id
        task_id = data.get("task_id")
        title = data.get("title")
        description = data.get("description")
        await bot.send_message(callback_query.from_user.id, TextMessages.TASK_UPDATED_SUCCESSFULLY)
        service = TaskService()
        await service.update_task_service(task_id, user_id, title, description)
        await cmd_menu(callback_query.message)
    elif callback_query.data == "cancel_updated_task":
        await bot.send_message(callback_query.from_user.id, TextMessages.TASK_UPDATED_CANCELLED)
        await cmd_menu(callback_query.message)


@dp.callback_query(lambda callback_query: callback_query.data == "return_to_menu")
async def return_to_menu(callback_query: types.CallbackQuery) -> None:
    await cmd_menu(callback_query.message)
