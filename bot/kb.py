from aiogram.types import InlineKeyboardButton, InlineKeyboardMarkup


class Kb:
    # Клавиатура в меню
    menu_kb = InlineKeyboardMarkup(
        inline_keyboard=[
            [InlineKeyboardButton(text="Добавить задачу", callback_data="add_task")],
            [InlineKeyboardButton(text="Список задач", callback_data="tasks_list")],
        ]
    )

    # Клавиатура для подтверждения добавления задачи
    confirm_task_addition_kb = InlineKeyboardMarkup(
        inline_keyboard=[
            [
                InlineKeyboardButton(text="Сохранить", callback_data="save_task"),
                InlineKeyboardButton(text="Отменить", callback_data="cancel_task"),
            ]
        ]
    )

    # Клавиатура для подтверждения обновления задачи
    confirm_task_update_kb = InlineKeyboardMarkup(
        inline_keyboard=[
            [
                InlineKeyboardButton(text="Да", callback_data="save_updated_task"),
                InlineKeyboardButton(text="Нет", callback_data="cancel_updated_task"),
            ]
        ]
    )

    # Клавиатура для возврата в меню
    return_to_menu_kb = InlineKeyboardMarkup(
        inline_keyboard=[[InlineKeyboardButton(text="\U00002190 Назад в меню", callback_data="return_to_menu")]]
    )


class KbCls:
    @classmethod
    def tasks_list_kb(cls, task_id: int) -> InlineKeyboardMarkup:
        # Клавиатура для списка задач и возврата в меню
        kb = InlineKeyboardMarkup(
            inline_keyboard=[
                [InlineKeyboardButton(text="Удалить задачу", callback_data=f"delete_task_{task_id}")],
                [InlineKeyboardButton(text="Изменить задачу", callback_data=f"edit_task_{task_id}")],
                [InlineKeyboardButton(text="\U00002190 К списку", callback_data="tasks_list")],
            ]
        )
        return kb
