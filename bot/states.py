from aiogram.fsm.state import State, StatesGroup


class MenuState(StatesGroup):
    ADDING_TITLE = State()
    ADDING_DESCRIPTION = State()
    CONFIRM_TASK_ADDITION = State()
    EDITING_TASK_TITLE = State()
    EDITING_TASK_DESCRIPTION = State()
    CONFIRM_TASK_UPDATE = State()
