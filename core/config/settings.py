import os

import yaml

from core.config.schema import Settings


def get_settings(p: str) -> Settings:
    with open(os.path.abspath(p)) as f:
        return Settings.parse_obj(yaml.safe_load(f))


current_directory = os.getcwd()
relative_path = os.path.join(current_directory, "config.yml")
settings = get_settings(relative_path)
