from sqlalchemy.ext.asyncio import AsyncSession

from core.connection.db_connection import get_session


class BaseDAO:
    @staticmethod
    def get_session() -> AsyncSession:
        return get_session()
