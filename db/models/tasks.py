from sqlalchemy import BigInteger, Column, Integer, String

from core.connection.db_connection import Base


class Task(Base):
    __tablename__ = "tasks"

    id = Column(Integer, primary_key=True)
    user_id = Column(BigInteger, nullable=False)
    title = Column(String, nullable=False)
    description = Column(String, nullable=True)
