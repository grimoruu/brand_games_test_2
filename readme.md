# TaskUp

Бот "Управление задачами" представляет собой инструмент для управления списком задач через Telegram. Пользователи могут
добавлять, просматривать, редактировать и удалять задачи непосредственно из мессенджера.

## Установка и Запуск

1. Клонирование репозитория:

```bash
git clone https://gitlab.com/grimoruu/brand_games_test_2.git
```

2. Директория проекта:

```bash
cd brand_games_test_2/
```

3. Установка зависимостей poetry:

```bash
poetry install
```

4. Сборка и запуск докеров:

```bash
docker compose build
```

```bash
docker compose up -d
```

5. Миграция

```bash
alembic revision --autogenerate -m "init"
alembic upgrade head
```

6. Запуск бота

```bash
python3 main.py
```

Бот будет доступен по адресу tg @brand_games_task_bot
